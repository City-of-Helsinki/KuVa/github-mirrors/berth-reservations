from django.apps import AppConfig


class HarborsConfig(AppConfig):
    name = "harbors"
