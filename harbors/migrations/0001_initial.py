# Generated by Django 2.1.2 on 2018-11-07 06:58

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import parler.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [("munigeo", "0004_delete_old_translations")]

    operations = [
        migrations.CreateModel(
            name="BoatType",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "identifier",
                    models.CharField(
                        max_length=100, unique=True, verbose_name="Unique identifier"
                    ),
                ),
            ],
            options={
                "verbose_name": "boat type",
                "verbose_name_plural": "boat types",
                "ordering": ("id",),
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name="BoatTypeTranslation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language_code",
                    models.CharField(
                        db_index=True, max_length=15, verbose_name="Language"
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        help_text="Name of the boat type",
                        max_length=200,
                        verbose_name="name",
                    ),
                ),
                (
                    "master",
                    models.ForeignKey(
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="translations",
                        to="harbors.BoatType",
                    ),
                ),
            ],
            options={
                "verbose_name": "boat type Translation",
                "db_table": "harbors_boattype_translation",
                "db_tablespace": "",
                "managed": True,
                "default_permissions": (),
            },
        ),
        migrations.CreateModel(
            name="Harbor",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "identifier",
                    models.CharField(
                        help_text="Unique string to identify the harbor, e.g. `elaintarhanlahti` for Eläintarhanlahti",
                        max_length=100,
                        unique=True,
                        verbose_name="Unique identifier",
                    ),
                ),
                (
                    "servicemap_id",
                    models.CharField(
                        blank=True,
                        help_text="ID in the Servicemap system",
                        max_length=10,
                        null=True,
                        verbose_name="Servicemap ID",
                    ),
                ),
                (
                    "zip_code",
                    models.CharField(
                        blank=True, max_length=10, null=True, verbose_name="Postal code"
                    ),
                ),
                (
                    "phone",
                    models.CharField(
                        blank=True,
                        max_length=30,
                        null=True,
                        verbose_name="Phone number",
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        blank=True, max_length=100, null=True, verbose_name="Email"
                    ),
                ),
                (
                    "www_url",
                    models.URLField(
                        blank=True, max_length=400, null=True, verbose_name="WWW link"
                    ),
                ),
                (
                    "location",
                    django.contrib.gis.db.models.fields.PointField(
                        null=True, srid=4326, verbose_name="Location"
                    ),
                ),
                (
                    "image_file",
                    models.ImageField(
                        blank=True,
                        null=True,
                        upload_to="harbors/",
                        verbose_name="Image file",
                    ),
                ),
                (
                    "image_link",
                    models.URLField(
                        blank=True, max_length=400, null=True, verbose_name="Image link"
                    ),
                ),
                ("mooring", models.BooleanField(default=False, verbose_name="Mooring")),
                (
                    "electricity",
                    models.BooleanField(default=False, verbose_name="Electricity"),
                ),
                ("water", models.BooleanField(default=False, verbose_name="Water")),
                (
                    "waste_collection",
                    models.BooleanField(default=False, verbose_name="Waste collection"),
                ),
                ("gate", models.BooleanField(default=False, verbose_name="Gate")),
                (
                    "lighting",
                    models.BooleanField(default=False, verbose_name="Lighting"),
                ),
                (
                    "number_of_places",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Number of places"
                    ),
                ),
                (
                    "maximum_width",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Maximum berth width"
                    ),
                ),
                (
                    "maximum_length",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Maximum berth length"
                    ),
                ),
                (
                    "maximum_depth",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Maximum berth depth"
                    ),
                ),
                (
                    "municipality",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="harbors",
                        to="munigeo.Municipality",
                        verbose_name="Municipality",
                    ),
                ),
                (
                    "suitable_boat_types",
                    models.ManyToManyField(
                        related_name="harbors",
                        to="harbors.BoatType",
                        verbose_name="Suitable boat types",
                    ),
                ),
            ],
            options={
                "verbose_name": "harbor",
                "verbose_name_plural": "harbors",
                "ordering": ("id",),
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name="HarborTranslation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language_code",
                    models.CharField(
                        db_index=True, max_length=15, verbose_name="Language"
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        help_text="Name of the harbor",
                        max_length=200,
                        verbose_name="name",
                    ),
                ),
                (
                    "street_address",
                    models.CharField(
                        help_text="Street address of the harbor",
                        max_length=200,
                        verbose_name="street address",
                    ),
                ),
                (
                    "master",
                    models.ForeignKey(
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="translations",
                        to="harbors.Harbor",
                    ),
                ),
            ],
            options={
                "verbose_name": "harbor Translation",
                "db_table": "harbors_harbor_translation",
                "db_tablespace": "",
                "managed": True,
                "default_permissions": (),
            },
        ),
        migrations.AlterUniqueTogether(
            name="harbortranslation", unique_together={("language_code", "master")}
        ),
        migrations.AlterUniqueTogether(
            name="boattypetranslation", unique_together={("language_code", "master")}
        ),
    ]
