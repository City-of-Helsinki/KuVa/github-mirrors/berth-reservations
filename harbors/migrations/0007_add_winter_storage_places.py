# Generated by Django 2.1.7 on 2019-04-04 08:36

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import harbors.models
import parler.models


class Migration(migrations.Migration):

    dependencies = [
        ("munigeo", "0004_delete_old_translations"),
        ("harbors", "0006_delete_identifier_fields"),
    ]

    operations = [
        migrations.CreateModel(
            name="WinterStorageArea",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "servicemap_id",
                    models.CharField(
                        blank=True,
                        help_text="ID in the Servicemap system",
                        max_length=10,
                        null=True,
                        verbose_name="Servicemap ID",
                    ),
                ),
                (
                    "zip_code",
                    models.CharField(
                        blank=True, max_length=10, null=True, verbose_name="Postal code"
                    ),
                ),
                (
                    "phone",
                    models.CharField(
                        blank=True,
                        max_length=30,
                        null=True,
                        verbose_name="Phone number",
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        blank=True, max_length=100, null=True, verbose_name="Email"
                    ),
                ),
                (
                    "www_url",
                    models.URLField(
                        blank=True, max_length=400, null=True, verbose_name="WWW link"
                    ),
                ),
                (
                    "location",
                    django.contrib.gis.db.models.fields.PointField(
                        blank=True, null=True, srid=4326, verbose_name="Location"
                    ),
                ),
                (
                    "image_file",
                    models.ImageField(
                        blank=True,
                        null=True,
                        storage=harbors.models.OverwriteStorage(),
                        upload_to=harbors.models.get_winter_area_media_folder,
                        verbose_name="Image file",
                    ),
                ),
                (
                    "image_link",
                    models.URLField(
                        blank=True, max_length=400, null=True, verbose_name="Image link"
                    ),
                ),
                (
                    "repair_area",
                    models.BooleanField(default=False, verbose_name="Repair area"),
                ),
                (
                    "electricity",
                    models.BooleanField(default=False, verbose_name="Electricity"),
                ),
                ("water", models.BooleanField(default=False, verbose_name="Water")),
                ("gate", models.BooleanField(default=False, verbose_name="Gate")),
                (
                    "summer_storage_for_docking_equipment",
                    models.BooleanField(
                        default=False,
                        verbose_name="Summer storage for docking equipment",
                    ),
                ),
                (
                    "summer_storage_for_trailers",
                    models.BooleanField(
                        default=False, verbose_name="Summer storage for trailers"
                    ),
                ),
                (
                    "summer_storage_for_boats",
                    models.BooleanField(
                        default=False, verbose_name="Summer storage for boats"
                    ),
                ),
                (
                    "number_of_marked_places",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Number of marked places"
                    ),
                ),
                (
                    "max_width",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Maximum place width"
                    ),
                ),
                (
                    "max_length",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Maximum place length"
                    ),
                ),
                (
                    "number_of_section_spaces",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Number of section places"
                    ),
                ),
                (
                    "max_length_of_section_spaces",
                    models.PositiveSmallIntegerField(
                        blank=True,
                        null=True,
                        verbose_name="Maximum length of section spaces",
                    ),
                ),
                (
                    "number_of_unmarked_spaces",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Number of unmarked places"
                    ),
                ),
                (
                    "municipality",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="winter_storage_areas",
                        to="munigeo.Municipality",
                        verbose_name="Municipality",
                    ),
                ),
            ],
            options={"abstract": False},
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name="WinterStorageAreaTranslation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language_code",
                    models.CharField(
                        db_index=True, max_length=15, verbose_name="Language"
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        blank=True,
                        help_text="Name of the area",
                        max_length=200,
                        verbose_name="name",
                    ),
                ),
                (
                    "street_address",
                    models.CharField(
                        blank=True,
                        help_text="Street address of the area",
                        max_length=200,
                        verbose_name="street address",
                    ),
                ),
                (
                    "master",
                    models.ForeignKey(
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="translations",
                        to="harbors.WinterStorageArea",
                    ),
                ),
            ],
            options={
                "verbose_name": "winter storage area Translation",
                "db_table": "harbors_winterstoragearea_translation",
                "db_tablespace": "",
                "managed": True,
                "default_permissions": (),
            },
        ),
        migrations.AlterUniqueTogether(
            name="winterstorageareatranslation",
            unique_together={("language_code", "master")},
        ),
    ]
