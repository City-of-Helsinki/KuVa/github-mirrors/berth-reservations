# Generated by Django 2.2 on 2019-05-31 08:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("harbors", "0011_add_disabled_flag")]

    operations = [
        migrations.AlterField(
            model_name="harbor",
            name="municipality",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to="munigeo.Municipality",
                verbose_name="Municipality",
            ),
        ),
        migrations.AlterField(
            model_name="winterstoragearea",
            name="municipality",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to="munigeo.Municipality",
                verbose_name="Municipality",
            ),
        ),
    ]
