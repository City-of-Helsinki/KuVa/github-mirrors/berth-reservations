# Generated by Django 2.2.6 on 2020-03-10 14:31

import django.core.files.storage
from django.db import migrations, models
import resources.models


class Migration(migrations.Migration):

    dependencies = [
        ("resources", "0005_add_map_field"),
    ]

    operations = [
        migrations.AlterField(
            model_name="harbor",
            name="image_file",
            field=models.ImageField(
                blank=True,
                null=True,
                storage=django.core.files.storage.FileSystemStorage(),
                upload_to=resources.models.get_harbor_media_folder,
                verbose_name="image file",
            ),
        ),
        migrations.AlterField(
            model_name="winterstoragearea",
            name="image_file",
            field=models.ImageField(
                blank=True,
                null=True,
                storage=django.core.files.storage.FileSystemStorage(),
                upload_to=resources.models.get_winter_area_media_folder,
                verbose_name="image file",
            ),
        ),
    ]
